import { useReducer } from "react";
import { todoReducer, initialState, TodoActionKind } from "./state/todoReducer";
import { Todo } from "types";
import "./App.css";
import { TodoEdit } from "./components/TodoEdit";
import { TodoItems } from "./components/TodoItems";

function App() {
  const [state, dispatch] = useReducer(todoReducer, initialState);

  const addItem = (item: string) => {
    dispatch({
      type: TodoActionKind.ADD,
      payload: { done: false, text: item },
    });
  };

  const updateItem = (item: Todo) => {
    dispatch({ type: TodoActionKind.UPDATE, payload: item });
  };
  const deleteItem = (id: number) => {
    dispatch({ type: TodoActionKind.DELETE, payload: id });
  };

  return (
    <>
      <div>
        <TodoEdit addItem={addItem} />
      </div>
      <TodoItems
        items={state.items}
        updateItem={updateItem}
        deleteItem={deleteItem}
      ></TodoItems>
    </>
  );
}

export default App;
