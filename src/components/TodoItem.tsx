import { useRef, useState } from "react";
import { Todo } from "types";

export const TodoItem = ({
  item,
  updateItem,
  deleteItem,
}: {
  item: Todo;
  updateItem: (item: Todo) => void;
  deleteItem: (id: number) => void;
}) => {
  const [editing, setEditing] = useState(false);
  const [newText, setNewText] = useState("");
  const inputId = `item_${item.id}`;
  const buttonRef = useRef<HTMLButtonElement>(null);
  return (
    <li>
      {editing ? (
        <form
          onSubmit={(e) => {
            console.log("here");
            e.preventDefault();
            updateItem({ ...item, text: newText });
            setEditing(false);
          }}
        >
          <input
            type="text"
            value={newText}
            onChange={(e) => setNewText(e.target.value)}
            onKeyDown={(e) => e.key === "Enter" && buttonRef.current?.click()}
          />
          <button onClick={() => setEditing(false)}>Cancel</button>
          <button
            ref={buttonRef}
            type="submit"
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            Done
          </button>
        </form>
      ) : (
        <>
          <input
            onChange={() => updateItem({ ...item, done: !item.done })}
            id={inputId}
            type="checkbox"
            checked={item.done}
          />
          <label htmlFor={inputId}>{item.text}</label>
          <button
            onClick={() => {
              setEditing(true);
              setNewText(item.text);
            }}
          >
            Edit
          </button>
          <button onClick={() => deleteItem(item.id)}>Delete</button>
        </>
      )}
    </li>
  );
};
