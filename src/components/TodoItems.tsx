import React from "react";
import { Todo } from "types";
import { TodoItem } from "./TodoItem";

export const TodoItems = ({
  items,
  updateItem,
  deleteItem,
}: {
  items: Todo[];
  updateItem: (item: Todo) => void;
  deleteItem: (id: number) => void;
}) => {
  return (
    <ul>
      {items.map((e) => (
        <TodoItem
          key={e.id}
          item={e}
          updateItem={updateItem}
          deleteItem={deleteItem}
        ></TodoItem>
      ))}
    </ul>
  );
};
