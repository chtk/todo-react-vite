import { useState } from "react";

export const TodoEdit = ({ addItem }: { addItem: (item: string) => void }) => {
  const [newItem, setNewItem] = useState<string>("");
  return (
    <div>
      <div>
        <input
          type="text"
          value={newItem}
          onChange={(e) => setNewItem(e.target.value)}
        />
      </div>
      <div>
        <button
          onClick={(e) => {
            addItem(newItem);
            setNewItem("");
          }}
          disabled={!newItem}
        >
          Voeg toe
        </button>
      </div>
    </div>
  );
};
