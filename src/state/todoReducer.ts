import { Todo } from "types";

export interface TodoState {
  nextId: number;
  items: Todo[];
}

export enum TodoActionKind {
  ADD = "add",
  UPDATE = "update",
  DELETE = "delete",
}

export type TodoAction =
  | {
      type: TodoActionKind.ADD;
      payload: Omit<Todo, "id">;
    }
  | { type: TodoActionKind.UPDATE; payload: Pick<Todo, "id"> & Partial<Todo> }
  | { type: TodoActionKind.DELETE; payload: number };

export const initialState: TodoState = {
  nextId: 3,
  items: [
    { done: true, id: 1, text: "Item 1" },
    { done: false, id: 2, text: "Item 2" },
    { done: false, id: 3, text: "Item 3" },
  ],
};

export const todoReducer = (
  state: TodoState,
  action: TodoAction
): TodoState => {
  const { type, payload } = action;
  switch (type) {
    case TodoActionKind.ADD:
      const nextId = state.nextId++;
      const item = { ...payload, id: nextId };
      return { nextId: nextId, items: [...state.items, item] };
    case TodoActionKind.UPDATE:
      return {
        nextId: state.nextId,
        items: state.items.map((e) =>
          e.id === payload.id ? { ...e, ...payload } : e
        ),
      };
    case TodoActionKind.DELETE:
      return { ...state, items: state.items.filter((e) => e.id !== payload) };
  }
};
